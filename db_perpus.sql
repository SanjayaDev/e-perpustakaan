-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 02 Jul 2020 pada 08.36
-- Versi Server: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_perpus`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_access`
--

CREATE TABLE `tb_access` (
  `access_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_access`
--

INSERT INTO `tb_access` (`access_id`, `menu_id`, `role_id`) VALUES
(1, 2, 1),
(7, 12, 1),
(12, 15, 1),
(13, 15, 2),
(14, 16, 1),
(15, 16, 2),
(16, 17, 1),
(17, 17, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_buku`
--

CREATE TABLE `tb_buku` (
  `buku_id` int(11) NOT NULL,
  `buku_judul` varchar(255) NOT NULL,
  `buku_noSKU` varchar(255) NOT NULL,
  `buku_penulis` varchar(255) NOT NULL,
  `buku_penerbit` varchar(255) NOT NULL,
  `buku_tahunTerbit` varchar(255) NOT NULL,
  `buku_tebal` varchar(255) NOT NULL,
  `buku_foto` varchar(255) NOT NULL,
  `buku_kategori` int(11) NOT NULL,
  `buku_stok` int(11) NOT NULL,
  `buku_status` int(11) NOT NULL,
  `buku_jual` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_buku`
--

INSERT INTO `tb_buku` (`buku_id`, `buku_judul`, `buku_noSKU`, `buku_penulis`, `buku_penerbit`, `buku_tahunTerbit`, `buku_tebal`, `buku_foto`, `buku_kategori`, `buku_stok`, `buku_status`, `buku_jual`) VALUES
(10, 'Codeigniter', '5421545121', 'Budi Raharjo', '1', '2018', '542', 'Ci1.jpg', 3, 10, 1, 2),
(11, 'Matematika kelas 12 SMK', '5451215412121', 'Sri Rayahu, S. Pd', '2', '2018', '520', 'bse-a_5c3c432277858959670002.jpg', 2, 5, 1, 2),
(12, 'Bahasa Indonesia Kelas 12 SMK', '555484521', 'Suryadi, S. Pd', '2', '2018', '540', 'bse-a_5c3c172c7d809104757019.jpg', 2, 2, 1, 2),
(13, 'Codeigniter 3', '8545452245', 'Betha Sidik', '1', '2019', '548', 'Codeigniter.jpg', 3, 4, 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_denda`
--

CREATE TABLE `tb_denda` (
  `denda_id` int(11) NOT NULL,
  `denda_harga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_denda`
--

INSERT INTO `tb_denda` (`denda_id`, `denda_harga`) VALUES
(1, 30000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_kategori`
--

CREATE TABLE `tb_kategori` (
  `kategori_id` int(11) NOT NULL,
  `kategori_judul` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_kategori`
--

INSERT INTO `tb_kategori` (`kategori_id`, `kategori_judul`) VALUES
(2, 'Pendidikan'),
(3, 'Programming');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_menu`
--

CREATE TABLE `tb_menu` (
  `menu_id` int(11) NOT NULL,
  `menu_judul` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_menu`
--

INSERT INTO `tb_menu` (`menu_id`, `menu_judul`) VALUES
(2, 'Menu'),
(12, 'Petugas'),
(15, 'User'),
(16, 'Data Buku'),
(17, 'Manajemen Buku');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_peminjaman`
--

CREATE TABLE `tb_peminjaman` (
  `peminjaman_id` int(11) NOT NULL,
  `peminjaman_user` varchar(255) NOT NULL,
  `peminjaman_buku` int(20) NOT NULL,
  `peminjaman_jumlah` int(11) NOT NULL,
  `peminjaman_dari` date NOT NULL,
  `peminjaman_sampai` date NOT NULL,
  `peminjaman_kembali` date NOT NULL,
  `peminjaman_denda` varchar(255) NOT NULL,
  `peminjaman_status` int(11) NOT NULL,
  `peminjaman_noId` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_peminjaman`
--

INSERT INTO `tb_peminjaman` (`peminjaman_id`, `peminjaman_user`, `peminjaman_buku`, `peminjaman_jumlah`, `peminjaman_dari`, `peminjaman_sampai`, `peminjaman_kembali`, `peminjaman_denda`, `peminjaman_status`, `peminjaman_noId`) VALUES
(9, '3', 10, 2, '2020-06-30', '2020-07-06', '2020-07-14', '160000', 2, 149292),
(10, '3', 10, 4, '2020-06-30', '2020-07-13', '2020-07-13', '0', 2, 48310),
(12, '3', 10, 5, '2020-06-30', '2020-07-06', '2020-07-08', '60000', 2, 384247);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_penerbit`
--

CREATE TABLE `tb_penerbit` (
  `penerbit_id` int(11) NOT NULL,
  `penerbit_judul` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_penerbit`
--

INSERT INTO `tb_penerbit` (`penerbit_id`, `penerbit_judul`) VALUES
(1, 'Informatika'),
(2, 'Kemdikbud');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_sub`
--

CREATE TABLE `tb_sub` (
  `sub_id` int(11) NOT NULL,
  `sub_menu` int(11) NOT NULL,
  `sub_judul` varchar(255) NOT NULL,
  `sub_link` varchar(255) NOT NULL,
  `sub_icon` varchar(255) NOT NULL,
  `sub_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_sub`
--

INSERT INTO `tb_sub` (`sub_id`, `sub_menu`, `sub_judul`, `sub_link`, `sub_icon`, `sub_status`) VALUES
(3, 2, 'Menu', 'menu', 'fas fa-fw fa-bars', 1),
(4, 2, 'Sub Menu', 'subMenu', 'fas fa-fw fa-ellipsis-v', 1),
(6, 12, 'Data Petugas', 'dataPetugas', 'fas fa-fw fa-user', 1),
(13, 15, 'Data User', 'dataUser', 'fas fa-fw fa-user', 1),
(14, 16, 'List Buku', 'listBuku', 'fas fa-fw fa-book', 1),
(15, 16, 'Data Penerbit', 'dataPenerbit', 'fas fa-fw fa-bookmark', 1),
(16, 16, 'List Kategori', 'listKategori', 'fas fa-fw fa-list', 1),
(17, 17, 'Data Peminjam', 'dataPeminjaman', 'fas fa-fw fa-book-reader', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_user`
--

CREATE TABLE `tb_user` (
  `user_id` int(11) NOT NULL,
  `user_noId` int(20) NOT NULL,
  `user_nama` varchar(255) NOT NULL,
  `user_username` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_role` int(11) NOT NULL,
  `user_noHP` varchar(20) NOT NULL,
  `user_email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_user`
--

INSERT INTO `tb_user` (`user_id`, `user_noId`, `user_nama`, `user_username`, `user_password`, `user_role`, `user_noHP`, `user_email`) VALUES
(3, 254212421, 'Mohammad Sanjaya', 'anjay', '$2y$10$wtBKAQ9jO0Qtk.PqZxzcteRDaph7purAM6A4E69KhfAOG7CKDZYaS', 1, '081216542515', 'wow@mail.com'),
(5, 792695, 'Reza Riyaldi', 'reza', '$2y$10$q6BWHoGFj7/eZ/Sm/dtIMeWQOiwSWVEPOfch34U/BNLL6DstO0V4e', 3, '08485745142', 'reza@mail.com'),
(6, 888978, 'Wahyu Adam', 'away123', '$2y$10$/mLc0K8Kn3hpvmfU.uyPwuCmyyBp5EdlL0SGn.4KY2X.zh2FyOi5G', 2, '081221545125', 'away@mail.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_access`
--
ALTER TABLE `tb_access`
  ADD PRIMARY KEY (`access_id`);

--
-- Indexes for table `tb_buku`
--
ALTER TABLE `tb_buku`
  ADD PRIMARY KEY (`buku_id`);

--
-- Indexes for table `tb_denda`
--
ALTER TABLE `tb_denda`
  ADD PRIMARY KEY (`denda_id`);

--
-- Indexes for table `tb_kategori`
--
ALTER TABLE `tb_kategori`
  ADD PRIMARY KEY (`kategori_id`);

--
-- Indexes for table `tb_menu`
--
ALTER TABLE `tb_menu`
  ADD PRIMARY KEY (`menu_id`);

--
-- Indexes for table `tb_peminjaman`
--
ALTER TABLE `tb_peminjaman`
  ADD PRIMARY KEY (`peminjaman_id`);

--
-- Indexes for table `tb_penerbit`
--
ALTER TABLE `tb_penerbit`
  ADD PRIMARY KEY (`penerbit_id`);

--
-- Indexes for table `tb_sub`
--
ALTER TABLE `tb_sub`
  ADD PRIMARY KEY (`sub_id`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_access`
--
ALTER TABLE `tb_access`
  MODIFY `access_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `tb_buku`
--
ALTER TABLE `tb_buku`
  MODIFY `buku_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `tb_denda`
--
ALTER TABLE `tb_denda`
  MODIFY `denda_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_kategori`
--
ALTER TABLE `tb_kategori`
  MODIFY `kategori_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_menu`
--
ALTER TABLE `tb_menu`
  MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `tb_peminjaman`
--
ALTER TABLE `tb_peminjaman`
  MODIFY `peminjaman_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tb_penerbit`
--
ALTER TABLE `tb_penerbit`
  MODIFY `penerbit_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_sub`
--
ALTER TABLE `tb_sub`
  MODIFY `sub_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
