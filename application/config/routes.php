<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['login'] = 'welcome';
$route['register'] = 'welcome/register';

$route['admin'] = 'A/index';
$route['menu'] = 'A/menu';
$route['editMenu/(.+)'] = 'A/editMenu/$1';
$route['hapusMenu/(.+)'] = 'A/hapusMenu/$1';
$route['prosesEditMenu'] = 'A/prosesEditMenu';

$route['addAccess'] = 'A/addAccess';
$route['editAccess/(.+)'] = 'A/editAccess/$1';
$route['hapusAccess/(.+)'] = 'A/hapusAccess/$1';
$route['prosesEditAccess'] = 'A/prosesEditAccess';

$route['subMenu'] = 'A/subMenu';
$route['addSubMenu'] = 'A/addSubMenu';
$route['editSub/(.+)'] = 'A/editSub/$1';
$route['updateSub'] = 'A/updateSub';
$route['hapusSub/(.+)'] = 'A/hapusSub/$1';

$route['dataPetugas'] = 'A/dataPetugas';
$route['addPetugas'] = 'A/addPetugas';
$route['editPetugas/(.+)'] = 'A/editPetugas/$1';
$route['updatePetugas'] = 'A/updatePetugas';
$route['hapusPetugas/(.+)'] = 'A/hapusPetugas/$1';

$route['dataUser'] = 'User/dataUser';
$route['addUser'] = 'User/addUser';
$route['editUser/(.+)'] = 'User/editUser/$1';
$route['updateUser'] = 'User/updateUser';
$route['hapusUser/(.+)'] = 'User/hapusUser/$1';


$route['dataPenerbit'] = 'Buku/dataPenerbit';
$route['addPenerbit'] = 'Buku/addPenerbit';
$route['editPenerbit/(.+)'] = 'Buku/editPenerbit/$1';
$route['updatePenerbit'] = 'Buku/updatePenerbit';
$route['hapusPenerbit/(.+)'] = 'Buku/hapusPenerbit/$1';

$route['listKategori'] = 'Buku/listKategori';
$route['addKategori'] = 'Buku/addKategori';
$route['editKategori/(.+)'] = 'Buku/editKategori/$1';
$route['updateKategori'] = 'Buku/updateKategori';
$route['hapusKategori/(.+)'] = 'Buku/hapusKategori/$1';

$route['listBuku'] = 'Buku/listBuku';
$route['addBuku'] = 'Buku/addBuku';
$route['editBuku/(.+)'] = 'Buku/editBuku/$1';
$route['updateBuku'] = 'Buku/updateBuku';
$route['hapusBuku/(.+)'] = 'Buku/hapusBuku/$1';

$route['denda'] = 'Pinjam/denda';

$route['editUser'] = 'profile/editUser';
$route['cetakUser'] = 'profile/cetakUser';

$route['gantiPassword'] = 'profile/gantiPassword';
$route['logout'] = 'profile/logout';

$route['dataPeminjaman'] = 'Pinjam/dataPeminjaman';
$route['addPeminjaman'] = 'Pinjam/addPeminjaman';
$route['batalkanPem/(.+)'] = 'Pinjam/batalkanPem/$1';
$route['hapusPem/(.+)'] = 'Pinjam/hapusPem/$1';
$route['kembaliPem/(.+)'] = 'Pinjam/kembaliPem/$1';
$route['updateKemPem'] = 'Pinjam/updateKemPem';