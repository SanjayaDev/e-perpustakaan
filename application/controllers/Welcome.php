<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		$this->form_validation->set_rules('username', 'Username', 'required|min_length[4]',[
			'required' => 'Wajib masukan username!',
			'min_length' => 'Masukan minimal 4 karakter!'
		]);
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[4]',[
			'required' => 'Wajib masukan password!',
			'min_length' => 'Masukan minimal 4 karakter!'
		]);
		
		
		if ($this->form_validation->run() == FALSE) {
			$data = ['title' => 'Login -  Sistem Informasi Perpustakaan'];
			$this->load->view('v_login',$data);
		} else {
			$this->prosesLogin();
		}		
	}

	private function prosesLogin()
	{
		$user 		= html_escape($this->input->post('username',true));
		$pass 		= html_escape($this->input->post('password',true));

		$cek = $this->M_data->editData(['user_username' => $this->db->escape_str($user)],'tb_user')->row();

		if($cek) {
			if(password_verify($pass,$cek->user_password)) {
				if($cek->user_role == 1){
					$sesi = [
						'id' => $cek->user_id,
						'role' => $cek->user_role,
						'status' => TRUE
					];
					
					$this->session->set_userdata($sesi);
					redirect('admin');
				} elseif($cek->user_role == 2){
					$sesi = [
						'id' => $cek->user_id,
						'role' => $cek->user_role,
						'status' => TRUE
					];
					
					$this->session->set_userdata($sesi);
					redirect('profile');
				} elseif($cek->user_role == 3){
					$sesi = [
						'id' => $cek->user_id,
						'role' => $cek->user_role,
						'status' => TRUE
					];
					
					$this->session->set_userdata($sesi);
					redirect('user');
				}
			} else {
				$this->session->set_flashdata('pesan', '<script>sweet("Gagal Login!","Password yang anda masukan salah!","error","Tutup");</script>');
			
				redirect('login');
			}
		} else {
			$this->session->set_flashdata('pesan', '<script>sweet("Gagal Login!","Username anda belum terdaftar!","error","Tutup");</script>');
			
			redirect('login');
		}
	}

	public function register()
	{
		$this->form_validation->set_rules('nama', 'Nama', 'required',[
			'required' => 'Wajib untuk masukan nama!'
		]);
		$this->form_validation->set_rules('user', 'Username', 'required|is_unique[tb_user.user_username]',[
			'required' => 'Wajib untuk masukan username!',
			'is_unique' => 'Username sudah terdaftar!'
		]);
		$this->form_validation->set_rules('mail', 'Email', 'required|valid_email|is_unique[tb_user.user_email]',[
			'required' => 'Wajib untuk masukan email!',
			'valid_email' => 'Masukan format email dengan benar!',
			'is_unique' => 'Email sudah didaftarkan!'
		]);
		$this->form_validation->set_rules('noHp', 'Nomor HP', 'required|numeric',[
			'required' => 'Wajib untuk masukan email!',
			'decimal' => 'Masukan nomor HP dengan angka!'
		]);
		$this->form_validation->set_rules('pass', 'Password', 'required|matches[pas1]',[
			'required' => 'Wajib untuk masukan password!',
			'matches' => 'Password tidak sesuai!'
		]);
		$this->form_validation->set_rules('pas1', 'Ulangi Password', 'required|matches[pass]',[
			'required' => 'Wajib untuk masukan ulangi password!',
			'matches' => 'Password tidak sesuai!'
		]);

		
		if($this->form_validation->run() == FALSE) {
			$data = ['title' => 'Register -  Sistem Informasi Perpustakaan'];
			$this->load->view('v_regis',$data);
		} else {
			$this->prosesRegis();
		}		
	}

	private function prosesRegis()
	{
		$nama 				= html_escape($this->input->post('nama', true));
		$user 				= html_escape($this->input->post('user', true));
		$mail 				= html_escape($this->input->post('mail', true));
		$pass 				= html_escape($this->input->post('pass', true));
		$noHp 				= html_escape($this->input->post('noHp', true));
		$id 					= rand(1, 1000000);

		$data = [
			'user_noId' => $id,
			'user_nama' => $this->db->escape_str($nama),
			'user_username' => $this->db->escape_str($user),
			'user_password' => password_hash($pass, PASSWORD_DEFAULT),
			'user_role' => 3,
			'user_noHP' => $this->db->escape_str($noHp),
			'user_email' => $this->db->escape_str($mail)
		];

		$this->M_data->insertData($data,'tb_user');

		$this->session->set_flashdata('pesan','<script>sweet("Sukses Register","Akun anda berhasil diregistrasi, silahkan login!","success","Tutup");</script>');
		
		redirect('login');
	}
}
