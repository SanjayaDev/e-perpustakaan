<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    protek_login();
    if($this->session->userdata('role') == 3) {
      redirect('block');
    }
  }

  public function dataUser() 
  {
    $data = [
      'title' => 'Data User',
      'u' => $this->M_data->editData(['user_id' => $this->session->userdata('id')],'tb_user')->row(),
      'menu' => $this->M_data->joinMenu()->result_array(),
      'user' => $this->M_data->editData(['user_role' => 3],'tb_user')->result()
    ];
    $this->load->view('template/v_head', $data);
    $this->load->view('admin/v_dataUser', $data);
    $this->load->view('template/v_footer');
  }

  public function addUser()
  {
    $this->form_validation->set_rules('nama', 'Nama', 'required',[
			'required' => 'Wajib untuk masukan nama!'
		]);
		$this->form_validation->set_rules('username', 'Username', 'required|is_unique[tb_user.user_username]',[
			'required' => 'Wajib untuk masukan username!',
			'is_unique' => 'Username sudah terdaftar!'
		]);
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[tb_user.user_email]',[
			'required' => 'Wajib untuk masukan email!',
			'valid_email' => 'Masukan format email dengan benar!',
			'is_unique' => 'Email sudah didaftarkan!'
		]);
		$this->form_validation->set_rules('noHP', 'Nomor HP', 'required|numeric',[
			'required' => 'Wajib untuk masukan email!',
			'decimal' => 'Masukan nomor HP dengan angka!'
		]);
		$this->form_validation->set_rules('password', 'Password', 'required',[
			'required' => 'Wajib untuk masukan password!',
    ]);
    
    if ($this->form_validation->run() == FALSE) {
      $this->session->set_flashdata('pesan', '<script>sweet("Gagal menambahkan!","Data petugas gagal ditambahkan! Pastikan form di isi dengan lengkap dan benar!","error","Tutup")</script>');
      redirect('dataUser');
    } else {
      $this->prosesAddUser();
    }    
  }

  private function prosesAddUser()
  {
    $nama 				= html_escape($this->input->post('nama', true));
		$user 				= html_escape($this->input->post('username', true));
		$mail 				= html_escape($this->input->post('email', true));
		$pass 				= html_escape($this->input->post('password', true));
    $noHp 				= html_escape($this->input->post('noHP', true));
    $id           = rand(1,1000000);
    
    $data = [
      'user_noId' => $id,
			'user_nama' => $this->db->escape_str($nama),
			'user_username' => $this->db->escape_str($user),
			'user_password' => password_hash($pass, PASSWORD_DEFAULT),
			'user_role' => 3,
			'user_noHP' => $this->db->escape_str($noHp),
			'user_email' => $this->db->escape_str($mail)
		];

		$this->M_data->insertData($data,'tb_user');

		$this->session->set_flashdata('pesan','<script>sweet("Sukses menambahkan","Data petugas berhasil ditambahkan!","success","Tutup");</script>');
		
		redirect('dataUser');
  }

  public function editUser($id)
  {
    $i = (int)$id;
    $data = [
      'title' => 'Edit Data User',
      'u' => $this->M_data->editData(['user_id' => $this->session->userdata('id')],'tb_user')->row(),
      'menu' => $this->M_data->joinMenu()->result_array(),
      'p' => $this->M_data->editData(['user_id' => $id],'tb_user')->row()
    ];
    $this->load->view('template/v_head', $data);
    $this->load->view('admin/v_editDataUser', $data);
    $this->load->view('template/v_footer');
  }

  public function updateUser()
  {
    $this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('noHP', 'Nomor HP', 'required|numeric');  

    $id   = html_escape((int)$this->input->post('id'));
    $pass = html_escape($this->input->post('password'));

    
    if ($this->form_validation->run() == FALSE) {
      $this->session->set_flashdata('pesan','<script>sweet("Gagal mengupdate","Data petugas gagal diupdate! Periksa kembali form anda!","error","Tutup");</script>');		
		  redirect('editUser/'.$id);
    } else {
      if($pass != '') {
        $this->updateUserPass();
      } else {
        $this->updateUserAct();
      }
    }    
  }

  private function updateUserPass()
  {
    $id           = html_escape((int)$this->input->post('id'));
    $nama 				= html_escape($this->input->post('nama', true));
    $pass         = html_escape($this->input->post('password'));
		$mail 				= html_escape($this->input->post('email', true));
    $noHp 				= html_escape($this->input->post('noHP', true));
  
    $data = [
      'user_nama' => $this->db->escape_str($nama),
      'user_password' => password_hash($pass, PASSWORD_DEFAULT),
			'user_noHP' => $this->db->escape_str($noHp),
			'user_email' => $this->db->escape_str($mail)
    ];
    
    $where = ['user_id' => $this->db->escape_str($id)];
    $this->M_data->updateData($data,$where,'tb_user');
    $this->session->set_flashdata('pesan','<script>sweet("Sukses mengupdate","Data petugas berhasil diupdate!","success","Tutup");</script>');
		
		redirect('dataUser');
  }

  private function updateUserAct()
  {
    $id           = html_escape((int)$this->input->post('id'));
    $nama 				= html_escape($this->input->post('nama', true));
		$mail 				= html_escape($this->input->post('email', true));
    $noHp 				= html_escape($this->input->post('noHP', true));
  
    $data = [
			'user_nama' => $this->db->escape_str($nama),
			'user_noHP' => $this->db->escape_str($noHp),
			'user_email' => $this->db->escape_str($mail)
    ];
    
    $where = ['user_id' => $this->db->escape_str($id)];
    $this->M_data->updateData($data,$where,'tb_user');
    $this->session->set_flashdata('pesan','<script>sweet("Sukses mengupdate","Data petugas berhasil diupdate!","success","Tutup");</script>');
		
		redirect('dataUser');
  }

  public function hapusUser($id) 
  {
    $i = (int)$id;
    $this->M_data->deleteData(['user_id' => $i],'tb_user');
    $this->session->set_flashdata('pesan', '<script>sweet("Berhasil menghapus!","Data petugas berhasil dihapus!","success","Tutup")</script>');
    redirect('dataUser');
  }
}