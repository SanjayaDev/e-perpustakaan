<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Buku extends CI_Controller {  

  public function __construct()
  {
    parent::__construct();
    protek_login();  
    if($this->session->userdata('role') == 3) {
      redirect('block');
    }  
  }

  public function dataPenerbit()
  {
    $data = [
      'title' => 'Data Penerbit',
      'u' => $this->M_data->editData(['user_id' => $this->session->userdata('id')],'tb_user')->row(),
      'menu' => $this->M_data->joinMenu()->result_array(),
      'penerbit' => $this->M_data->getData('tb_penerbit')->result()
    ];
    $this->load->view('template/v_head', $data);
    $this->load->view('admin/v_dataPenerbit', $data);
    $this->load->view('template/v_footer');
  }

  public function addPenerbit()
  {
    $this->form_validation->set_rules('penerbit', 'Penerbit', 'required');
    
    if ($this->form_validation->run() == FALSE) {
      $this->session->set_flashdata('pesan', '<script>sweet("Gagal menambahkan!","Data penerbit gagal ditambahkan!","error","Tutup")</script>');
      redirect('dataPenerbit');
    } else {
      $this->addPenerbitAct();
    }    
  }

  private function addPenerbitAct()
  {
    $penerbit     = html_escape($this->input->post('penerbit',true));

    $data = ['penerbit_judul' => $penerbit];
    $this->M_data->insertData($data,'tb_penerbit');
    $this->session->set_flashdata('pesan', '<script>sweet("Sukses menambahkan!","Data penerbit sukses ditambahkan!","success","Tutup")</script>');
    redirect('dataPenerbit');
  }

  public function editPenerbit($id)
  {
    $i = (int)$id;
    $data = [
      'title' => 'Data Penerbit',
      'u' => $this->M_data->editData(['user_id' => $this->session->userdata('id')],'tb_user')->row(),
      'menu' => $this->M_data->joinMenu()->result_array(),
      'p' => $this->M_data->editData(['penerbit_id' => $i],'tb_penerbit')->row()
    ];
    $this->load->view('template/v_head', $data);
    $this->load->view('admin/v_editPenerbit', $data);
    $this->load->view('template/v_footer');
  }

  public function updatePenerbit()
  {
    $this->form_validation->set_rules('penerbit', 'Penerbit', 'required');
    $i = html_escape((int)$this->input->post('id',true));
    
    if ($this->form_validation->run() == FALSE) {
      $this->session->set_flashdata('pesan', '<script>sweet("Gagal mengupdate!","Data penerbit gagal diupdate!","error","Tutup")</script>');
      redirect('editPenerbit/'.$i);
    } else {
      $this->updatePenerbitAct(); 
    }
  }

  private function updatePenerbitAct()
  {
    $id   = html_escape((int)$this->input->post('id',true));
    $menu = html_escape($this->input->post('penerbit',true));

    $data = ['penerbit_judul' => $this->db->escape_str($menu)];
    $where = ['penerbit_id' => $this->db->escape_str($id)];
    $this->M_data->updateData($data,$where,'tb_penerbit');

    $this->session->set_flashdata('pesan', '<script>sweet("Sukses mengupdate!","Data penerbit sukses diupdate!","success","Tutup")</script>');
    redirect('dataPenerbit');
  }

  public function hapusPenerbit($id)
  {
    $i = (int)$id;
    $this->M_data->deleteData(['penerbit_id' => $i],'tb_penerbit');
    $this->session->set_flashdata('pesan', '<script>sweet("Berhasil menghapus!","Data penerbit berhasil dihapus!","success","Tutup")</script>');
    redirect('dataPenerbit');
  } 

  public function listKategori()
  {
    $data = [
      'title' => 'List Kategori',
      'u' => $this->M_data->editData(['user_id' => $this->session->userdata('id')],'tb_user')->row(),
      'menu' => $this->M_data->joinMenu()->result_array(),
      'kategori' => $this->M_data->getData('tb_kategori')->result()
    ];
    $this->load->view('template/v_head', $data);
    $this->load->view('admin/v_listKategori', $data);
    $this->load->view('template/v_footer');
  }

  public function addKategori()
  {
    $this->form_validation->set_rules('kategori', 'Kategori', 'required');
    
    if ($this->form_validation->run() == FALSE) {
      $this->session->set_flashdata('pesan', '<script>sweet("Gagal menambahkan!","Data kategori gagal ditambahkan!","error","Tutup")</script>');
      redirect('listKategori');
    } else {
      $this->addKategoriAct();
    }
  }

  private function addKategoriAct()
  {
    $kat     = html_escape($this->input->post('kategori',true));

    $data = ['kategori_judul' => $kat];
    $this->M_data->insertData($data,'tb_kategori');
    $this->session->set_flashdata('pesan', '<script>sweet("Sukses menambahkan!","Data kategori sukses ditambahkan!","success","Tutup")</script>');
    redirect('listKategori');
  }

  public function editKategori($id)
  {
    $i = (int)$id;
    $data = [
      'title' => 'Edit Kategori',
      'u' => $this->M_data->editData(['user_id' => $this->session->userdata('id')],'tb_user')->row(),
      'menu' => $this->M_data->joinMenu()->result_array(),
      'k' => $this->M_data->editData(['kategori_id' => $i],'tb_kategori')->row()
    ];
    $this->load->view('template/v_head', $data);
    $this->load->view('admin/v_editKategori', $data);
    $this->load->view('template/v_footer'); 
  }

  public function updateKategori()
  {
    $this->form_validation->set_rules('kategori', 'Kategori', 'required');
    $i = html_escape((int)$this->input->post('id',true));
    
    if ($this->form_validation->run() == FALSE) {
      $this->session->set_flashdata('pesan', '<script>sweet("Gagal mengupdate!","Data kategori gagal diupdate!","error","Tutup")</script>');
      redirect('editKategori/'.$i);
    } else {
      $this->updateKategoriAct(); 
    }
  }

  private function updateKategoriAct()
  {
    $id   = html_escape((int)$this->input->post('id',true));
    $kat  = html_escape($this->input->post('kategori',true));

    $data = ['kategori_judul' => $this->db->escape_str($kat)];
    $where = ['kategori_id' => $this->db->escape_str($id)];
    $this->M_data->updateData($data,$where,'tb_kategori');

    $this->session->set_flashdata('pesan', '<script>sweet("Sukses mengupdate!","Data kategori sukses diupdate!","success","Tutup")</script>');
    redirect('listKategori');
  }

  public function hapusKategori($id)
  {
    $i = (int)$id;
    $this->M_data->deleteData(['kategori_id' => $i],'tb_kategori');
    $this->session->set_flashdata('pesan', '<script>sweet("Berhasil menghapus!","Data kategori berhasil dihapus!","success","Tutup")</script>');
    redirect('listKategori');
  }

  public function listBuku()
  {
    $data = [
      'title' => 'List Buku',
      'u' => $this->M_data->editData(['user_id' => $this->session->userdata('id')],'tb_user')->row(),
      'menu' => $this->M_data->joinMenu()->result_array(),
      'penerbit' => $this->M_data->getData('tb_penerbit')->result(),
      'kategori' => $this->M_data->getData('tb_kategori')->result(),
      'buku' => $this->M_data->joinBuku()->result()    
    ];
    $this->load->view('template/v_head', $data);
    $this->load->view('admin/v_listBuku', $data);
    $this->load->view('template/v_footer');
  }

  public function addBuku()
  {
    $this->form_validation->set_rules('judul', 'Judul Buku', 'required');
    $this->form_validation->set_rules('sku', 'Nomor SKU', 'required');
    $this->form_validation->set_rules('penulis', 'Penulis', 'required');
    $this->form_validation->set_rules('penerbit', 'Penerbit', 'required');
    $this->form_validation->set_rules('kategori', 'Kategori', 'required');
    $this->form_validation->set_rules('tahun', 'Tahun Terbit', 'required');
    $this->form_validation->set_rules('tebal', 'Tebal Buku', 'required');
    $this->form_validation->set_rules('status', 'Status Buku', 'required');
    $this->form_validation->set_rules('jual', 'Status Jual Buku', 'required');
    $this->form_validation->set_rules('stok', 'Stok Buku', 'required');
    
    
    if ($this->form_validation->run() == FALSE) {
      $this->session->set_flashdata('pesan', '<script>sweet("Gagal menambahkan!","Data buku gagal ditambahkan! Isi dengan lengkap!","error","Tutup")</script>');
      redirect('listBuku');
    } else {
      $this->addBukuAct();
    }           
  }

  private function addBukuAct()
  {
    $foto     = $_FILES['foto']['name'];

    // echo $foto;

    if($foto != '') {
      $judul          = html_escape($this->input->post('judul',true));
      $sku            = html_escape($this->input->post('sku',true));
      $penulis        = html_escape($this->input->post('penulis',true));
      $penerbit       = html_escape($this->input->post('penerbit',true));
      $kategori       = html_escape($this->input->post('kategori',true));
      $tahun          = html_escape($this->input->post('tahun',true));
      $tebal          = html_escape($this->input->post('tebal',true));
      $status         = html_escape($this->input->post('status',true));
      $jual           = html_escape($this->input->post('jual',true));
      $stok           = html_escape($this->input->post('stok',true));
      $foto           = rand(1,10000).$_FILES['foto']['name'];

      $config['upload_path']          = './vendor/img/buku/';
      $config['allowed_types']        = 'jpeg|jpg|png';
      $config['max_size']             = 2000;
      $config['max_width']            = 1024;
      $config['max_height']           = 1024;


      $this->load->library('upload');
      $this->upload->initialize($config);

      if(!$this->upload->do_upload('foto')) {
        $error = $this->session->set_flashdata('pesan', '<script>sweet("Gagal menambahkan!","Gagal mengupload gambar!","error","Tutup")</script>');        
        redirect(base_url('listBuku', $error));
      } else {
        $foto = $this->upload->data('file_name');
      }

      $data = [
        'buku_judul' => $this->db->escape_str($judul),
        'buku_noSKU' => $this->db->escape_str($sku),
        'buku_penulis' => $this->db->escape_str($penulis),
        'buku_penerbit' => $this->db->escape_str($penerbit),
        'buku_tahunTerbit' => $this->db->escape_str($tahun),
        'buku_tebal' => $this->db->escape_str($tebal),
        'buku_foto' => $this->db->escape_str($foto),
        'buku_kategori' => $this->db->escape_str($kategori),
        'buku_stok' => $this->db->escape_str($stok),
        'buku_status' => $this->db->escape_str($status),
        'buku_jual' => $this->db->escape_str($jual)
      ];

      $this->M_data->insertData($data,'tb_buku');
      $this->session->set_flashdata('pesan', '<script>sweet("Sukses menambahkan!","Data buku sukses ditambahkan! Isi dengan lengkap!","success","Tutup")</script>');
      redirect('listBuku');
    } else {
      $judul          = html_escape($this->input->post('judul',true));
      $sku            = html_escape($this->input->post('sku',true));
      $penulis        = html_escape($this->input->post('penulis',true));
      $penerbit       = html_escape($this->input->post('penerbit',true));
      $tahun          = html_escape($this->input->post('tahun',true));
      $tebal          = html_escape($this->input->post('tebal',true));
      $status         = html_escape($this->input->post('status',true));
      $jual           = html_escape($this->input->post('jual',true));
      $stok           = html_escape($this->input->post('stok',true));

      $data = [
        'buku_judul' => $this->db->escape_str($judul),
        'buku_noSKU' => $this->db->escape_str($sku),
        'buku_penulis' => $this->db->escape_str($penulis),
        'buku_penerbit' => $this->db->escape_str($penerbit),
        'buku_tahunTerbit' => $this->db->escape_str($tahun),
        'buku_tebal' => $this->db->escape_str($tebal),
        'buku_foto' => 'default.jpg',
        'buku_kategori' => $this->db->escape_str($kategori),
        'buku_stok' => $this->db->escape_str($stok),
        'buku_status' => $this->db->escape_str($status),
        'buku_jual' => $this->db->escape_str($jual)
      ];

      $this->M_data->insertData($data,'tb_buku');
      $this->session->set_flashdata('pesan', '<script>sweet("Sukses menambahkan!","Data buku sukses ditambahkan!","success","Tutup")</script>');
      redirect('listBuku');
    }
  }

  public function editBuku($id)
  {
    $i = (int)$id;
    $data = [
      'title' => 'Dashboard',
      'u' => $this->M_data->editData(['user_id' => $this->session->userdata('id')],'tb_user')->row(),
      'menu' => $this->M_data->joinMenu()->result_array(),
      'b' => $this->M_data->editData(['buku_id' => $id],'tb_buku')->row(),
      'penerbit' => $this->M_data->getData('tb_penerbit')->result(),
      'kategori' => $this->M_data->getData('tb_kategori')->result()
    ];
    $this->load->view('template/v_head', $data);
    $this->load->view('admin/v_editBuku', $data);
    $this->load->view('template/v_footer');
  }

  public function updateBuku()
  {
    $this->form_validation->set_rules('judul', 'Judul Buku', 'required');
    $this->form_validation->set_rules('sku', 'Nomor SKU', 'required');
    $this->form_validation->set_rules('penulis', 'Penulis', 'required');
    $this->form_validation->set_rules('penerbit', 'Penerbit', 'required');
    $this->form_validation->set_rules('kategori', 'Kategori', 'required');
    $this->form_validation->set_rules('tahun', 'Tahun Terbit', 'required');
    $this->form_validation->set_rules('tebal', 'Tebal Buku', 'required');
    $this->form_validation->set_rules('status', 'Status Buku', 'required');
    $this->form_validation->set_rules('jual', 'Status Jual Buku', 'required');
    $this->form_validation->set_rules('stok', 'Stok Buku', 'required');

    $id          = html_escape((int)$this->input->post('id',true));

    
    if ($this->form_validation->run() == FALSE) {
      $this->session->set_flashdata('pesan', '<script>sweet("Gagal mengupdate!","Data buku gagal diupdate! Isi dengan lengkap!","error","Tutup")</script>');
      redirect('editBuku/'.$id);
    } else {
      $this->updateBukuAct();
    }    
  }

  private function updateBukuAct()
  {
    $foto     = $_FILES['foto']['name'];

    //echo $foto;

    if($foto != '') {
      $id             = html_escape((int)$this->input->post('id',true));
      $judul          = html_escape($this->input->post('judul',true));
      $sku            = html_escape($this->input->post('sku',true));
      $penulis        = html_escape($this->input->post('penulis',true));
      $penerbit       = html_escape($this->input->post('penerbit',true));
      $kategori       = html_escape($this->input->post('kategori',true));
      $tahun          = html_escape($this->input->post('tahun',true));
      $tebal          = html_escape($this->input->post('tebal',true));
      $status         = html_escape($this->input->post('status',true));
      $jual           = html_escape($this->input->post('jual',true));
      $stok           = html_escape($this->input->post('stok',true));
      $foto           = rand(1,10000).$_FILES['foto']['name'];

      $cek = $this->M_data->editData(['buku_id' => $id],'tb_buku')->row();
      unlink('./vendor/img/buku/'.$cek->buku_foto);

      $config['upload_path']          = './vendor/img/buku/';
      $config['allowed_types']        = 'jpeg|jpg|png';
      $config['max_size']             = 2000;
      $config['max_width']            = 1024;
      $config['max_height']           = 1024;


      $this->load->library('upload');
      $this->upload->initialize($config);

      if(!$this->upload->do_upload('foto')) {
        $error = $this->session->set_flashdata('pesan', '<script>sweet("Gagal menambahkan!","Gagal mengupload gambar!","error","Tutup")</script>');        
        redirect(base_url('listBuku', $error));
      } else {
        $foto = $this->upload->data('file_name');
      }

      $data = [
        'buku_judul' => $this->db->escape_str($judul),
        'buku_noSKU' => $this->db->escape_str($sku),
        'buku_penulis' => $this->db->escape_str($penulis),
        'buku_penerbit' => $this->db->escape_str($penerbit),
        'buku_tahunTerbit' => $this->db->escape_str($tahun),
        'buku_tebal' => $this->db->escape_str($tebal),
        'buku_foto' => $this->db->escape_str($foto),
        'buku_kategori' => $this->db->escape_str($kategori),
        'buku_stok' => $this->db->escape_str($stok),
        'buku_status' => $this->db->escape_str($status),
        'buku_jual' => $this->db->escape_str($jual)
      ];
      $where = ['buku_id' => $id];

      $this->M_data->updateData($data,$where,'tb_buku');
      $this->session->set_flashdata('pesan', '<script>sweet("Sukses mengupdate!","Data buku sukses diupdate! Isi dengan lengkap!","success","Tutup")</script>');
      redirect('listBuku');
    }  else {
      $id             = html_escape((int)$this->input->post('id',true));
      $judul          = html_escape($this->input->post('judul',true));
      $sku            = html_escape($this->input->post('sku',true));
      $penulis        = html_escape($this->input->post('penulis',true));
      $penerbit       = html_escape($this->input->post('penerbit',true));
      $kategori       = html_escape($this->input->post('kategori',true));
      $tahun          = html_escape($this->input->post('tahun',true));
      $tebal          = html_escape($this->input->post('tebal',true));
      $status         = html_escape($this->input->post('status',true));
      $jual           = html_escape($this->input->post('jual',true));
      $stok           = html_escape($this->input->post('stok',true));

      $data = [
        'buku_judul' => $this->db->escape_str($judul),
        'buku_noSKU' => $this->db->escape_str($sku),
        'buku_penulis' => $this->db->escape_str($penulis),
        'buku_penerbit' => $this->db->escape_str($penerbit),
        'buku_tahunTerbit' => $this->db->escape_str($tahun),
        'buku_tebal' => $this->db->escape_str($tebal),
        'buku_kategori' => $this->db->escape_str($kategori),
        'buku_stok' => $this->db->escape_str($stok),
        'buku_status' => $this->db->escape_str($status),
        'buku_jual' => $this->db->escape_str($jual)
      ];
      $where = ['buku_id' => $id];

      $this->M_data->updateData($data,$where,'tb_buku');
      $this->session->set_flashdata('pesan', '<script>sweet("Sukses mengupdate!","Data buku sukses diupdate!","success","Tutup")</script>');
      redirect('listBuku');
    }
  }

  public function hapusBuku($id)
  {
    $i = (int)$id;
    $cek = $this->M_data->editData(['buku_id' => $i],'tb_buku')->row();
    unlink('./vendor/img/buku/'.$cek->buku_foto);
    $this->M_data->deleteData(['buku_id' => $i],'tb_buku');
    $this->session->set_flashdata('pesan', '<script>sweet("Berhasil menghapus!","Data buku berhasil dihapus!","success","Tutup")</script>');
    redirect('listBuku');
  }
}