<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pinjam extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    protek_login();
    if($this->session->userdata('role') == 3) {
      redirect('block');
    }
  }

  public function dataPeminjaman()
  {
    $data = [
      'title' => 'Data Peminjam',
      'u' => $this->M_data->editData(['user_id' => $this->session->userdata('id')],'tb_user')->row(),
      'menu' => $this->M_data->joinMenu()->result_array(),
      'peminjaman' => $this->M_data->joinPeminjaman()->result(),
      'user' => $this->M_data->getData('tb_user')->result(),
      'buku' => $this->M_data->editData('buku_stok>0','tb_buku')->result(),
      'd' => $this->M_data->getData('tb_denda')->row()
    ];
    $this->load->view('template/v_head', $data);
    $this->load->view('admin/v_dataPeminjaman', $data);
    $this->load->view('template/v_footer');
  }

  public function addPeminjaman()
  {
    $this->form_validation->set_rules('user', 'User', 'required');
    $this->form_validation->set_rules('buku', 'Buku', 'required');
    $this->form_validation->set_rules('jumlah', 'Jumlah Peminjaman', 'required');
    $this->form_validation->set_rules('pinjam', 'Tanggal Peminjaman', 'required');
    $this->form_validation->set_rules('kembali', 'Tanggal Kembali', 'required');
    
    if ($this->form_validation->run() == FALSE) {
      $this->session->set_flashdata('pesan', '<script>sweet("Gagal menambahkan!","Data peminjaman gagal ditambahkan! Isi form dengan lengkap!","error","Tutup")</script>');
      redirect('dataPeminjaman');
    } else {
      $this->addPeminjamanAct();
    }    
  }

  private function addPeminjamanAct()
  {
    $user         = html_escape($this->input->post('user',true));
    $buku         = html_escape($this->input->post('buku',true));
    $pinjam       = html_escape($this->input->post('pinjam',true));
    $kembali      = html_escape($this->input->post('kembali',true));
    $noId         = rand(1, 1000000);
    $jumlah       = html_escape($this->input->post('jumlah',true));
    

    $cek = $this->M_data->editData(['buku_id' => $buku],'tb_buku')->row();
    $kurang = $cek->buku_stok - $jumlah;

    $data = ['buku_stok' => $kurang];
    $where = ['buku_id' => $buku];

    $this->M_data->updateData($data,$where,'tb_buku');
    
  

    $data = [
      'peminjaman_user' => $this->db->escape_str($user),
      'peminjaman_buku' => $this->db->escape_str($buku),
      'peminjaman_jumlah' => $this->db->escape_str($jumlah),
      'peminjaman_dari' => $this->db->escape_str($pinjam),
      'peminjaman_sampai' => $this->db->escape_str($kembali),
      'peminjaman_denda' => 0,
      'peminjaman_status' => 1,
      'peminjaman_noId' => $noId
    ];

    $this->M_data->insertData($data,'tb_peminjaman');
    $this->session->set_flashdata('pesan', '<script>sweet("Sukses menambahkan!","Data peminjaman sukses ditambahkan! Isi dengan lengkap!","success","Tutup")</script>');
      redirect('dataPeminjaman');
  }

  public function batalkanPem($id)
  {
    $i = (int)$id;
    $cek = $this->M_data->editData(['peminjaman_id' => $i],'tb_peminjaman')->row();
    $buku = $this->M_data->editData(['buku_id' => $cek->peminjaman_buku],'tb_buku')->row();

    $data = ['buku_stok' => $buku->buku_stok + $cek->peminjaman_jumlah];
    $where = ['buku_id' => $cek->peminjaman_buku];
    $this->M_data->updateData($data,$where,'tb_buku');

    $data = ['peminjaman_status' => 3];
    $where = ['peminjaman_id' => $i];
    $this->M_data->updateData($data,$where,'tb_peminjaman');
    
    $this->session->set_flashdata('pesan', '<script>sweet("Peminjaman dibatalkan!","Peminjaman buku telah dibatalkan!","warning","Tutup")</script>');
    redirect('dataPeminjaman'); 
  }

  public function hapusPem($id)
  {
    $i = (int)$id;
    $this->M_data->deleteData(['peminjaman_id' => $i],'tb_peminjaman');
    $this->session->set_flashdata('pesan', '<script>sweet("Peminjaman dihapus!","Peminjaman buku telah dihapus!","success","Tutup")</script>');
    redirect('dataPeminjaman');
  }

  public function kembaliPem($id)
  {
    $i = (int)$id;
    $data = [
      'title' => 'Pengembalian Buku',
      'u' => $this->M_data->editData(['user_id' => $this->session->userdata('id')],'tb_user')->row(),
      'menu' => $this->M_data->joinMenu()->result_array(),
      'p' => $this->M_data->editData(['peminjaman_id' => $i],'tb_peminjaman')->row(),
      'user' => $this->M_data->getData('tb_user')->result(),
      'buku' => $this->M_data->editData('buku_stok>0','tb_buku')->result()
    ];
    $this->load->view('template/v_head', $data);
    $this->load->view('admin/v_kembaliPem', $data);
    $this->load->view('template/v_footer');
  }

  public function updateKemPem()
  {
    $id       = html_escape($this->input->post('id',true));
    $this->form_validation->set_rules('dikembalikan', 'Dikembalikan', 'required');

    // $cek = $this->form_validation->run();
    // var_dump($cek);
    
    if($this->form_validation->run() == FALSE) {
      $this->session->set_flashdata('pesan', '<script>sweet("Gagal mengupdate!","Data peminjaman gagal diupdate! Isi dengan lengkap!","error","Tutup")</script>');
      redirect('kembaliPem/'.$id);
    } else {
      $this->updateKemPemAct();
    }    
  }

  private function updateKemPemAct() 
  {
    $id       = html_escape($this->input->post('id',true));
    $tglKem   = html_escape($this->input->post('dikembalikan',true));

    $p = $this->M_data->editData(['peminjaman_id' => $id],'tb_peminjaman')->row();
    $d = $this->M_data->getData('tb_denda')->row();
    $b = $this->M_data->editData(['buku_id' => $p->peminjaman_buku],'tb_buku')->row();

    $sampai     = strtotime($p->peminjaman_sampai);
    $kembali    = strtotime($tglKem);

    $selisih    = abs(($sampai - $kembali)/(60*60*24));
    $denda      = $selisih * $d->denda_harga;
    

    $data = ['buku_stok' => $b->buku_stok + $p->peminjaman_jumlah];
    $where = ['buku_id' => $b->buku_id];
    $this->M_data->updateData($data,$where,'tb_buku');

    $data = [
      'peminjaman_status' => 2,
      'peminjaman_denda' => $denda,
      'peminjaman_kembali' => $tglKem
    ];
    $where = ['peminjaman_id' => $id];

    $this->M_data->updateData($data,$where,'tb_peminjaman');
    $this->session->set_flashdata('pesan', '<script>sweet("Sukses mengupdate!","Data peminjaman sukses diupdate!","success","Tutup")</script>');
      redirect('dataPeminjaman');
  }

  public function denda()
  {
    $this->form_validation->set_rules('denda', 'Harga Denda', 'required',[
      'required' => 'Wajib di isi'
    ]);  

    
    if ($this->form_validation->run() == FALSE) {
      $data = [
        'title' => 'Dashboard',
        'u' => $this->M_data->editData(['user_id' => $this->session->userdata('id')],'tb_user')->row(),
        'menu' => $this->M_data->joinMenu()->result_array(),
        'd' => $this->M_data->getData('tb_denda')->row()
      ];
      $this->load->view('template/v_head', $data);
      $this->load->view('admin/v_denda', $data);
      $this->load->view('template/v_footer');
    } else {
      $denda      = html_escape($this->input->post('denda',true));

      $data = ['denda_harga' => $this->db->escape_str($denda)];
      $where = ['denda_id' => 1];
      $this->M_data->updateData($data,$where,'tb_denda');

      $this->session->set_flashdata('pesan', '<script>sweet("Sukses mengupdate!","Data denda sukses diupdate!","success","Tutup")</script>');
      redirect('dataPeminjaman');
    }    
  }
}