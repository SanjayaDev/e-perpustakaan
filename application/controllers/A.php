<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class A extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    protek_login();
    if($this->session->userdata('role') != 1) {
      redirect('block');
    }
  }

  public function index()
  {
    $data = [
      'title' => 'Dashboard',
      'u' => $this->M_data->editData(['user_id' => $this->session->userdata('id')],'tb_user')->row(),
      'menu' => $this->M_data->joinMenu()->result_array()
    ];
    $this->load->view('template/v_head', $data);
    $this->load->view('admin/v_index', $data);
    $this->load->view('template/v_footer');    
  }

  public function menu()
  {
    $this->form_validation->set_rules('menu', 'Menu', 'required');    
    
    if ($this->form_validation->run() == FALSE) {
      $data = [
        'title' => 'Menu',
        'u' => $this->M_data->editData(['user_id' => $this->session->userdata('id')],'tb_user')->row(),
        'menu' => $this->M_data->joinMenu()->result_array(),
        'men' => $this->M_data->getData('tb_menu')->result(),
        'acc' => $this->M_data->joinAcc()->result(),
      ];
      $this->load->view('template/v_head', $data);
      $this->load->view('admin/v_menu', $data);
      $this->load->view('template/v_footer');
    } else {
      $this->addMenu();
    }    
  }

  private function addMenu()
  {
    $menu   = html_escape($this->input->post('menu',true));

    $data = ['menu_judul' => $this->db->escape_str($menu)];

    $this->M_data->insertData($data,'tb_menu');
    
    $this->session->set_flashdata('pesan', '<script>sweet("Berhasil menambahkan!","Data menu berhasil ditambahkan!","success","Tutup")</script>');
    redirect('menu');    
  }

  public function editMenu($id)
  {
    $data = [
      'title' => 'Edit Menu',
      'u' => $this->M_data->editData(['user_id' => $this->session->userdata('id')],'tb_user')->row(),
      'menu' => $this->M_data->joinMenu()->result_array(),
      'edit' => $this->M_data->editData(['menu_id' => $id],'tb_menu')->row_array()
    ];
    $this->load->view('template/v_head', $data);
    $this->load->view('admin/v_editMenu', $data);
    $this->load->view('template/v_footer');  
  }

  public function prosesEditMenu()
  {
    $this->form_validation->set_rules('menu', 'Menu Judul', 'required',['required' => 'Wajib masukan menu judul!']);
    $id     = html_escape((int)$this->input->post('id',true));
    $menu   = html_escape($this->input->post('menu', true));
    
    if ($this->form_validation->run() == FALSE) {
      $this->session->set_flashdata('pesan', '<script>sweet("Gagal mengupdate!","Data menu gagal diupdate! Isi form dengan lengkap!","error","Tutup")</script>');
      redirect('editMenu/'.$id); 
    } else {
      $data   = ['menu_judul' => $this->db->escape_str($menu)];
      $where  = ['menu_id' => $this->db->escape_str($id)];
      $this->M_data->updateData($data,$where,'tb_menu');

      $this->session->set_flashdata('pesan', '<script>sweet("Berhasil mengupdate!","Data menu berhasil diupdate!","success","Tutup")</script>');
      redirect('menu'); 
    }    
  }

  public function hapusMenu($id)
  {
    $i = (int)$id;
    $this->M_data->deleteData(['menu_id' => $i],'tb_menu');
    $this->M_data->deleteData(['menu_id' => $i],'tb_access');
    $this->session->set_flashdata('pesan', '<script>sweet("Berhasil menghapus!","Data menu berhasil dihapus!","success","Tutup")</script>');
    redirect('menu'); 
  }

  public function addAccess()
  {
    $this->form_validation->set_rules('menu', 'Menu Judul', 'required');
    $this->form_validation->set_rules('role', 'Hak Akses', 'required');

    
    if ($this->form_validation->run() == FALSE) {
      $this->session->set_flashdata('pesan', '<script>sweet("Gagal menambahkan!","Data access gagal ditambahkan!","error","Tutup")</script>');
      redirect('menu');
    } else {
      $menu     = html_escape($this->input->post('menu',true));
      $role     = html_escape($this->input->post('role',true));

      $data = [
        'menu_id' => $this->db->escape_str($menu),
        'role_id' => $this->db->escape_str($role)
      ];

      $this->M_data->insertData($data,'tb_access');
      $this->session->set_flashdata('pesan', '<script>sweet("Berhasil menambahkan!","Data access berhasil ditambahkan!","success","Tutup")</script>');
      redirect('menu');
    }    
  }

  public function editAccess($id)
  {
    $data = [
      'title' => 'Edit Sub Menu',
      'u' => $this->M_data->editData(['user_id' => $this->session->userdata('id')],'tb_user')->row(),
      'menu' => $this->M_data->joinMenu()->result_array(),
      'acc' => $this->M_data->editData(['access_id' => $id],'tb_access')->row(),
      'listMenu' => $this->M_data->getData('tb_menu')->result()
    ];
    $this->load->view('template/v_head', $data);
    $this->load->view('admin/v_editAccess', $data);
    $this->load->view('template/v_footer');
  }

  public function prosesEditAccess() 
  {
    $this->form_validation->set_rules('menu', 'Menu Judul', 'required');
    $this->form_validation->set_rules('role', 'Hak akses', 'required');
    $id   = html_escape((int)$this->input->post('id',true));

    
    if ($this->form_validation->run() == FALSE) {
      $this->session->set_flashdata('pesan', '<script>sweet("Gagal mengupdate!","Data access gagal diupdate!","error","Tutup")</script>');
      redirect('editSub/'.$id);
    } else {
      $menu   = html_escape($this->input->post('menu',true));
      $role   = html_escape($this->input->post('role',true));

      $data = [
        'menu_id' => $this->db->escape_str($menu),
        'role_id' => $this->db->escape_str($role)
      ];
      $where = ['access_id' => $id];
      $this->M_data->updateData($data,$where,'tb_access');

      $this->session->set_flashdata('pesan', '<script>sweet("Berhasil mengupdate!","Data access berhasil diupdate!","success","Tutup")</script>');
      redirect('menu'); 
    }    
  }

  public function hapusAccess($id)
  {
    $i = (int)$id;
    $this->M_data->deleteData(['access_id' => $i],'tb_access');
    $this->session->set_flashdata('pesan', '<script>sweet("Berhasil menghapus!","Data access berhasil dihapus!","success","Tutup")</script>');
    redirect('menu');
  }

  public function subMenu()
  {  
    $data = [
      'title' => 'Sub Menu',
      'u' => $this->M_data->editData(['user_id' => $this->session->userdata('id')],'tb_user')->row(),
      'menu' => $this->M_data->joinMenu()->result_array(),
      'sub' => $this->M_data->joinSub()->result(),
      'listMenu' => $this->M_data->getData('tb_menu')->result()
    ];
    $this->load->view('template/v_head', $data);
    $this->load->view('admin/v_subMenu', $data);
    $this->load->view('template/v_footer');        
  }

  public function addSubMenu()
  {
    $this->form_validation->set_rules('menu', 'Menu', 'required');
    $this->form_validation->set_rules('judul', 'Judul', 'required');
    $this->form_validation->set_rules('link', 'Link', 'required');
    $this->form_validation->set_rules('icon', 'Icon', 'required');
    $this->form_validation->set_rules('status', 'Status', 'required');

    
    if ($this->form_validation->run() == FALSE) {
      $this->session->set_flashdata('pesan', '<script>sweet("Gagal menambahkan!","Data sub menu gagal ditambahkan!","error","Tutup")</script>');
      redirect('subMenu');
    } else {
      $this->addSubMenuAction();
    }
  }

  private function addSubMenuAction()
  {
    $menu       = html_escape($this->input->post('menu',true));
    $judul      = html_escape($this->input->post('judul',true));
    $link       = html_escape($this->input->post('link',true));
    $icon       = html_escape($this->input->post('icon',true));
    $status     = html_escape($this->input->post('status',true));

    $data = [
      'sub_menu'      => $this->db->escape_str($menu),
      'sub_judul'     => $this->db->escape_str($judul),
      'sub_link'      => $this->db->escape_str($link),
      'sub_icon'      => $this->db->escape_str($icon),
      'sub_status'    => $this->db->escape_str($status)
    ];

    $this->M_data->insertData($data,'tb_sub');
    $this->session->set_flashdata('pesan', '<script>sweet("Berhasil menambahkan!","Data sub menu berhasil ditambahkan!","success","Tutup")</script>');
    redirect('subMenu');
  }

  public function editSub($id)
  {
    $data = [
      'title' => 'Edit Sub Menu',
      'u' => $this->M_data->editData(['user_id' => $this->session->userdata('id')],'tb_user')->row(),
      'menu' => $this->M_data->joinMenu()->result_array(),
      's' => $this->M_data->editData(['sub_id' => $id],'tb_sub')->row(),
      'listMenu' => $this->M_data->getData('tb_menu')->result()
    ];
    $this->load->view('template/v_head', $data);
    $this->load->view('admin/v_editSub', $data);
    $this->load->view('template/v_footer');
  }


  public function updateSub()
  {
    $this->form_validation->set_rules('menu', 'Menu', 'required',[
      'required' => 'Menu wajib dipilih!'
    ]);
    $this->form_validation->set_rules('judul', 'Judul', 'required',[
      'required' => 'Wajib masukan judul!'
    ]);
    $this->form_validation->set_rules('link', 'Link', 'required',[
      'required' => 'Wajib masukan link!'
    ]);
    $this->form_validation->set_rules('icon', 'Icon', 'required',[
      'required' => 'Wajib masukan icon!'
    ]);
    $this->form_validation->set_rules('status', 'Status', 'required',[
      'required' => 'Status wajib dipilih!'
    ]);

    $id = html_escape((int)$this->input->post('id',true));
    
    if ($this->form_validation->run() == FALSE) {
      $this->session->set_flashdata('pesan', '<script>sweet("Gagal mengupdate!","Data sub menu gagal diupdate! Isi form dengan lengkap","error","Tutup")</script>');
      redirect('editSub/'.$id);
    } else {
      $this->updateSubAct();
    }    
  }

  private function updateSubAct()
  {
    $id           = html_escape((int)$this->input->post('id',true));
    $menu         = html_escape($this->input->post('menu',true));
    $judul        = html_escape($this->input->post('judul',true));
    $link         = html_escape($this->input->post('link',true));
    $icon         = html_escape($this->input->post('icon',true));
    $status       = html_escape($this->input->post('status',true));

    $data   = [
      'sub_menu' => $this->db->escape_str($menu),
      'sub_judul' => $this->db->escape_str($judul),
      'sub_link' => $this->db->escape_str($link),
      'sub_icon' => $this->db->escape_str($icon),
      'sub_status' => $this->db->escape_str($status)      
    ];

    $where = ['sub_id' => $this->db->escape_str($id)];

    $this->M_data->updateData($data,$where,'tb_sub');
    $this->session->set_flashdata('pesan', '<script>sweet("Berhasil mengupdate!","Data sub menu berhasil diupdate!","success","Tutup")</script>');
    redirect('subMenu');
  }

  public function hapusSub($id)
  {
    $i = (int)$id;
    $this->M_data->deleteData(['sub_id' => $i],'tb_sub');
    $this->session->set_flashdata('pesan', '<script>sweet("Berhasil menghapus!","Data sub menu berhasil dihapus!","success","Tutup")</script>');
    redirect('subMenu');
  }

  public function dataPetugas() 
  {
    $data = [
      'title' => 'Data Petugas',
      'u' => $this->M_data->editData(['user_id' => $this->session->userdata('id')],'tb_user')->row(),
      'menu' => $this->M_data->joinMenu()->result_array(),
      'petugas' => $this->M_data->editData(['user_role !=' => 3],'tb_user')->result()
    ];
    $this->load->view('template/v_head', $data);
    $this->load->view('admin/v_dataPetugas', $data);
    $this->load->view('template/v_footer');
  }

  public function addPetugas()
  {
    $this->form_validation->set_rules('nama', 'Nama', 'required',[
			'required' => 'Wajib untuk masukan nama!'
		]);
		$this->form_validation->set_rules('username', 'Username', 'required|is_unique[tb_user.user_username]',[
			'required' => 'Wajib untuk masukan username!',
			'is_unique' => 'Username sudah terdaftar!'
		]);
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[tb_user.user_email]',[
			'required' => 'Wajib untuk masukan email!',
			'valid_email' => 'Masukan format email dengan benar!',
			'is_unique' => 'Email sudah didaftarkan!'
		]);
		$this->form_validation->set_rules('noHP', 'Nomor HP', 'required|numeric',[
			'required' => 'Wajib untuk masukan email!',
			'decimal' => 'Masukan nomor HP dengan angka!'
		]);
		$this->form_validation->set_rules('password', 'Password', 'required',[
			'required' => 'Wajib untuk masukan password!',
    ]);    
		$this->form_validation->set_rules('role', 'Role', 'required',[
			'required' => 'Wajib untuk masukan password!',
    ]);    
    
    if ($this->form_validation->run() == FALSE) {
      $this->session->set_flashdata('pesan', '<script>sweet("Gagal menambahkan!","Data petugas gagal ditambahkan! Pastikan form di isi dengan lengkap dan benar!","error","Tutup")</script>');
      redirect('dataPetugas');
    } else {
      $this->prosesAddPetugas();
    }    
  }

  private function prosesAddPetugas()
  {
    $nama 				= html_escape($this->input->post('nama', true));
		$user 				= html_escape($this->input->post('username', true));
		$mail 				= html_escape($this->input->post('email', true));
		$pass 				= html_escape($this->input->post('password', true));
    $noHp 				= html_escape($this->input->post('noHP', true));
    $role 				= html_escape($this->input->post('role', true));
    $id           = rand(1,1000000);
    
    $data = [
      'user_noId' => $id,
			'user_nama' => $this->db->escape_str($nama),
			'user_username' => $this->db->escape_str($user),
			'user_password' => password_hash($pass, PASSWORD_DEFAULT),
			'user_role' => $this->db->escape_str($role),
			'user_noHP' => $this->db->escape_str($noHp),
			'user_email' => $this->db->escape_str($mail)
		];

		$this->M_data->insertData($data,'tb_user');

		$this->session->set_flashdata('pesan','<script>sweet("Sukses menambahkan","Data petugas berhasil ditambahkan!","success","Tutup");</script>');
		
		redirect('dataPetugas');
  }

  public function editPetugas($id)
  {
    $i = (int)$id;
    $data = [
      'title' => 'Edit Data Petugas',
      'u' => $this->M_data->editData(['user_id' => $this->session->userdata('id')],'tb_user')->row(),
      'menu' => $this->M_data->joinMenu()->result_array(),
      'p' => $this->M_data->editData(['user_id' => $id],'tb_user')->row()
    ];
    $this->load->view('template/v_head', $data);
    $this->load->view('admin/v_editPetugas', $data);
    $this->load->view('template/v_footer');
  }

  public function updatePetugas()
  {
    $this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('noHP', 'Nomor HP', 'required|numeric');  
		$this->form_validation->set_rules('role', 'Role', 'required');  

    $id   = html_escape((int)$this->input->post('id'));
    $pass = html_escape($this->input->post('password'));

    $c =  $this->M_data->editData(['user_id' => $id],'tb_user')->row();

    
    if ($this->form_validation->run() == FALSE) {
      $this->session->set_flashdata('pesan','<script>sweet("Gagal mengupdate","Data petugas gagal diupdate! Periksa kembali form anda!","error","Tutup");</script>');		
		  redirect('editPetugas/'.$id);
    } else {
      if($pass != '') {
        $this->updatePetugasPass();
      } else {
        $this->updatePetugasAct();
      }
    }    
  }

  private function updatePetugasPass()
  {
    $id           = html_escape((int)$this->input->post('id'));
    $nama 				= html_escape($this->input->post('nama', true));
    $pass         = html_escape($this->input->post('password'));
		$mail 				= html_escape($this->input->post('email', true));
    $noHp 				= html_escape($this->input->post('noHP', true));
    $role 				= html_escape($this->input->post('role', true));
  
    $data = [
      'user_nama' => $this->db->escape_str($nama),
      'user_password' => password_hash($pass, PASSWORD_DEFAULT),
			'user_role' => $this->db->escape_str($role),
			'user_noHP' => $this->db->escape_str($noHp),
			'user_email' => $this->db->escape_str($mail)
    ];
    
    $where = ['user_id' => $this->db->escape_str($id)];
    $this->M_data->updateData($data,$where,'tb_user');
    $this->session->set_flashdata('pesan','<script>sweet("Sukses mengupdate","Data petugas berhasil diupdate!","success","Tutup");</script>');
		
		redirect('dataPetugas');
  }

  private function updatePetugasAct()
  {
    $id           = html_escape((int)$this->input->post('id'));
    $nama 				= html_escape($this->input->post('nama', true));
		$mail 				= html_escape($this->input->post('email', true));
    $noHp 				= html_escape($this->input->post('noHP', true));
    $role 				= html_escape($this->input->post('role', true));
  
    $data = [
			'user_nama' => $this->db->escape_str($nama),
			'user_role' => $this->db->escape_str($role),
			'user_noHP' => $this->db->escape_str($noHp),
			'user_email' => $this->db->escape_str($mail)
    ];
    
    $where = ['user_id' => $this->db->escape_str($id)];
    $this->M_data->updateData($data,$where,'tb_user');
    $this->session->set_flashdata('pesan','<script>sweet("Sukses mengupdate","Data petugas berhasil diupdate!","success","Tutup");</script>');
		
		redirect('dataPetugas');
  }

  public function hapusPetugas($id) 
  {
    $i = (int)$id;
    $this->M_data->deleteData(['user_id' => $i],'tb_user');
    $this->session->set_flashdata('pesan', '<script>sweet("Berhasil menghapus!","Data petugas berhasil dihapus!","success","Tutup")</script>');
    redirect('dataPetugas');
  }      
}