<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {  

  public function __construct()
  {
    parent::__construct();
    protek_login();    
  }

  public function index()
  {    
    $data = [
      'title' => 'Cetak Kartu Saya',
      'u' => $this->M_data->editData(['user_id' => $this->session->userdata('id')],'tb_user')->row(),
      'menu' => $this->M_data->joinMenu()->result_array()
    ];
    $this->load->view('template/v_head', $data);
    $this->load->view('admin/v_profile', $data);
    $this->load->view('template/v_footer');    
  }

  public function cetakUser()
  {
    $data = [
      'title' => 'Cetak Kartu Saya',
      'u' => $this->M_data->editData(['user_id' => $this->session->userdata('id')],'tb_user')->row(),
      'menu' => $this->M_data->joinMenu()->result_array()
    ];
    $this->load->view('admin/v_cetakUser',$data);
  }

  public function editUser() 
  {
    $this->form_validation->set_rules('nama', 'Nama', 'required',[
      'required' => 'Wajib masukan nama!'
    ]);
    $this->form_validation->set_rules('email', 'Email', 'required',[
      'required' => 'Wajib masukan Email!'
    ]);
    $this->form_validation->set_rules('noHP', 'Nomor HP', 'required',[
      'required' => 'Wajib masukan nomor hp!'
    ]);

    
    if ($this->form_validation->run() == FALSE) {
      $data = [
        'title' => 'Cetak Kartu Saya',
        'u' => $this->M_data->editData(['user_id' => $this->session->userdata('id')],'tb_user')->row(),
        'menu' => $this->M_data->joinMenu()->result_array()
      ];
      $this->load->view('template/v_head', $data);
      $this->load->view('admin/v_editUser', $data);
      $this->load->view('template/v_footer');
    } else {
      $this->prosesEdit();
    } 
  }

  private function prosesEdit()
  {
    $nama         = html_escape($this->input->post('nama',true));
    $email        = html_escape($this->input->post('email',true));
    $noHp         = html_escape($this->input->post('noHP',true));

    $data = [
      'user_nama' => $this->db->escape_str($nama),
      'user_email' => $this->db->escape_str($email),
      'user_noHP' => $this->db->escape_str($noHp)
    ];
    $where = ['user_id' => $this->session->userdata('id')];

    $this->M_data->updateData($data,$where,'tb_user');
    $this->session->set_flashdata('pesan', '<script>sweet("Sukses mengupdate!","Data profil sukses diupdate!","success","Tutup")</script>');
    redirect('profile');
  }

  public function gantiPassword()
  {
    $this->form_validation->set_rules('passLama', 'Password Lama', 'required',[
      'required' => 'Wajib masukan password lama anda!'
    ]);
    $this->form_validation->set_rules('passBaru', 'Password Baru', 'required|min_length[5]|max_length[12]|matches[passBaru1]',[
      'required' => 'Wajib masukan password baru!',
      'min_length' => 'Panjang karakter minimal 5!',
      'max_length' => 'Panjang karakter maximal 12',
      'matches' => 'Password tidak sesuai!'
    ]);
    $this->form_validation->set_rules('passBaru1', 'Password Baru', 'required|min_length[5]|max_length[12]|matches[passBaru]',[
      'required' => 'Wajib masukan password baru!',
      'min_length' => 'Panjang karakter minimal 5!',
      'max_length' => 'Panjang karakter maximal 12',
      'matches' => 'Password tidak sesuai!'
    ]);    

    
    if ($this->form_validation->run() == FALSE) {
      $data = [
        'title' => 'Ganti Password',
        'u' => $this->M_data->editData(['user_id' => $this->session->userdata('id')],'tb_user')->row(),
        'menu' => $this->M_data->joinMenu()->result_array()
      ];
      $this->load->view('template/v_head', $data);
      $this->load->view('admin/v_changePass', $data);
      $this->load->view('template/v_footer');
    } else {
      $this->prosesGantiPass();
    } 
  }

  private function prosesGantiPass()
  {
    $passLama       = html_escape($this->input->post('passLama'));
    $cek            = $this->M_data->editData(['user_id' => $this->session->userdata('id')],'tb_user')->row();

    if(password_verify($passLama,$cek->user_password)) {
      $passNew    = password_hash($this->input->post('passBaru'), PASSWORD_DEFAULT);
      
      $data = ['user_password' => $passNew];
      $where = ['user_id' => $this->session->userdata('id')];
      $this->M_data->updateData($data,$where,'tb_user');

      $this->session->set_flashdata('pesan', '<script>sweet("Sukses mengupdate!","Password sukses diupdate!","success","Tutup")</script>');
      redirect('gantiPassword');
    } else {
      $this->session->set_flashdata('pesan', '<script>sweet("Gagal mengganti password!","Password lama anda tidak sesuai!","error","Tutup")</script>');
      redirect('gantiPassword');
    }
  }

  public function logout()
  {    
    $this->session->set_flashdata('pesan', '<script>sweet("Logout!","Anda telah logout!","warning","Tutup")</script>');
    redirect('login');
  }
}