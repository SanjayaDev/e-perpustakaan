<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title><?= $title; ?></title>

  <!-- Custom fonts for this template-->
  <link href="<?= base_url('vendor/vendor/fontawesome-free/css/all.min.css') ?>" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<?= base_url('vendor/css/sb-admin-2.min.css') ?>" rel="stylesheet">

  
</head>

<body class="bg-gradient-primary">

  <div class="container">

        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-md-6 mx-auto">
            <div class="card o-hidden border-0 shadow-lg my-5">
              <div class="card-body p-0">
              <div class="p-5">
                <div class="text-center">
                  <h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>
                </div>
                <?= form_open('register','class="user" id="regis"') ?>
                  <div class="form-group">
                    <input type="text" name="nama" class="form-control form-control-user" id="nama" placeholder="Masukan nama lengkap" value="<?= set_value('nama'); ?>" required>
                    <?= form_error('nama','<small class="text-danger">','</small>') ?>
                  </div>
                  <div class="form-group">
                    <input type="email" name="mail" class="form-control form-control-user" id="mail" placeholder="Alamat email" value="<?= set_value('mail'); ?>" required>
                    <?= form_error('mail','<small class="text-danger">','</small>') ?>
                  </div>
                  <div class="form-group">
                    <input type="number" name="noHp" class="form-control form-control-user" id="noHp" placeholder="Nomor HP" value="<?= set_value('noHp'); ?>" required>
                    <?= form_error('noHp','<small class="text-danger">','</small>') ?>
                  </div>
                  <div class="form-group">
                    <input type="text" name="user" class="form-control form-control-user" id="user" placeholder="Username" value="<?= set_value('user'); ?>" required>
                    <?= form_error('user','<small class="text-danger">','</small>') ?>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                      <input type="password" name="pass" id="pass" class="form-control form-control-user" placeholder="Password" required>
                      <?= form_error('pass','<small class="text-danger">','</small>') ?>
                    </div>
                    <div class="col-sm-6">
                      <input type="password" name="pas1" id="pas1" class="form-control form-control-user" placeholder="Ulangi Password" required>
                      <?= form_error('pas1','<small class="text-danger">','</small>') ?>
                    </div>
                  </div>
                  <input type="submit" value="Register" class="btn btn-primary btn-user btn-block" onclick="valid()">
                </form>
                <hr>
                <div class="text-center">
                  <a class="small" href="<?= base_url('login') ?>">Already have an account? Login!</a>
                </div>
          </div>
        </div>
      </div>
    </div>

  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="<?= base_url('vendor/vendor/jquery/jquery.min.js'); ?>"></script>
  <script src="<?= base_url('vendor/vendor/bootstrap/js/bootstrap.min.js'); ?>"></script>
  <script src="<?= base_url('vendor/js/sweet.js'); ?>"></script>
  <script src="<?= base_url('vendor/js/custom.js'); ?>"></script>


</body>

</html>
