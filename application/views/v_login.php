<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title><?= $title; ?></title>

  <!-- Custom fonts for this template-->
  <link href="<?= base_url('vendor/vendor/fontawesome-free/css/all.min.css') ?>" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<?= base_url('vendor/css/sb-admin-2.min.css') ?>" rel="stylesheet">

  <script src="<?= base_url('vendor/js/sweet.js'); ?>"></script>
  <script src="<?= base_url('vendor/js/custom.js'); ?>"></script>

</head>

<body class="bg-gradient-primary">
  <?php
    echo $this->session->flashdata('pesan');
    $this->session->sess_destroy();
  ?>

  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-md-6">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-12">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">LOGIN</h1>
                  </div>
                  <?= form_open('login','class="user"'); ?>
                    <div class="form-group">
											<input type="text" class="form-control form-control-user" placeholder="Masukan username" name="username" required>
											<?= form_error('username','<small class="text-danger">','</small>') ?>
                    </div>
                    <div class="form-group">
											<input type="password" class="form-control form-control-user"  placeholder="Masukan Password" name="password" required>
											<?= form_error('password','<small class="text-danger">','</small>') ?>
                    </div>                    
                    <input type="submit" value="Login" class="btn btn-primary btn-user btn-block">                    
                  </form>
                  <hr>
                  <div class="text-center">
                    <a class="small" href="forgot-password.html">Forgot Password?</a>
                  </div>
                  <div class="text-center">
                    <a class="small" href="<?= base_url('register') ?>">Create an Account!</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="<?= base_url('vendor/vendor/jquery/jquery.min.js'); ?>"></script>
  <script src="<?= base_url('vendor/vendor/bootstrap/js/bootstrap.min.js'); ?>"></script>
  

</body>

</html>
