<?= $this->session->flashdata('pesan'); ?>
<div class="container-fluid">
  <h4>Pengembalian Buku</h4>
  <div class="row"> 
    <div class="col-md-8">
      <div class="card">
        <div class="card-body">
          <?= form_open('updateKemPem'); ?>
            <div class="form-group">
              <label>User</label>
              <select name="user" class="form-control" disabled>
                <?php foreach($user as $u) { ?>
                <option <?php if($u->user_id == $p->peminjaman_user) { echo 'selected'; } ?> value="<?= $u->user_id; ?>"><?= $u->user_nama; ?></option>
                <?php } ?>
              </select>
            </div>
            <div class="form-group">
              <label>Buku</label>
              <select name="buku" class="form-control" disabled>
                <?php foreach($buku as $b) { ?>
                <option <?php if($b->buku_id == $p->peminjaman_buku) { echo 'selected'; } ?> value="<?= $b->buku_id; ?>"><?= $b->buku_judul; ?></option>
                <?php } ?>
              </select>
            </div>
            <div class="form-group">
              <label>Jumlah Peminjaman</label>
              <input type="hidden" name="id" value="<?= $p->peminjaman_id; ?>">
              <input type="number" name="jumlah" class="form-control" value="<?= $p->peminjaman_jumlah; ?>" disabled>
            </div>
            <div class="form-group">
              <label>Tanggal Peminjaman</label>
              <input type="date" name="pinjam" class="form-control" value="<?= $p->peminjaman_dari; ?>" disabled>
            </div>
            <div class="form-group">
              <label>Tanggal Pengembalian</label>
              <input type="date" name="kembali" class="form-control" value="<?= $p->peminjaman_sampai; ?>" disabled>
            </div>
            <div class="form-group">
              <label>Tanggal Dikembalikan</label>
              <input type="date" name="dikembalikan" class="form-control">
            </div>
            <input type="submit" value="Simpan" class="btn btn-success btn-sm">
          <?= form_close() ?>
        </div>
      </div>
    </div>
  </div>
</div>