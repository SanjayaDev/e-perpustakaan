<?= $this->session->flashdata('pesan'); ?>
<div class="container-fluid">
  <h4>Data User</h4>
  <div class="card">
    <div class="card-body">
      <a href="" data-toggle="modal" data-target="#add" class="btn btn-primary btn-sm mb-3">Tambah User</a>
      <div class="table-responsive">
        <table class="table table-bordered table-hover" id="data">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Username</th>
              <th>Nomor HP</th>
              <th>Email</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php $no=1; foreach($user as $p) { ?>
            <tr>
              <td><?= $no++; ?></td>
              <td><?= $p->user_nama; ?></td>
              <td><?= $p->user_username; ?></td>
              <td><?= $p->user_noHP; ?></td>
              <td><?= $p->user_email; ?></td>
              <td>
                <a href="<?= base_url('editUser/'.$p->user_id); ?>" class="btn btn-info btn-sm">Edit</a>
                <a href="<?= base_url('hapusPetugas/'.$p->user_id); ?>" class="btn btn-danger btn-sm">Hapus</a>
              </td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="add">
  <div class="modal-dialog modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h5>Tambah User</h5>
        <button type="button" data-dismiss="modal" class="close">&times;</button>
      </div>
      <div class="modal-body">
        <?= form_open('addUser'); ?>
        <div class="form-group">
          <label>Nama</label>
          <input type="text" name="nama" class="form-control" required>
        </div>
        <div class="form-group">
          <label>Username</label>
          <input type="text" name="username" class="form-control" required>
        </div>
        <div class="form-group">
          <label>Password</label>
          <input type="password" name="password" class="form-control" required>
        </div>
          <div class="form-group">
            <label>Nomor HP</label>
            <input type="number" name="noHP" class="form-control" required>
          </div>
          <div class="form-group">
            <label>Email</label>
            <input type="email" name="email" class="form-control" required>
          </div>
        <input type="submit" value="Simpan" class="btn btn-success btn-sm">
        <?= form_close(); ?>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">Close</button>
      </div>
    </div>
  </div>
</div>