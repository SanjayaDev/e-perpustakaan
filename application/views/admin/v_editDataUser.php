<?= $this->session->flashdata('pesan'); ?>
<div class="container-fluid">
  <h4>Edit data user</h4>
  <div class="row">
    <div class="col-md-6">
      <div class="card">
        <div class="card-body">
          <?= form_open('updateUser'); ?>
          <div class="form-group">
            <label>Nama</label>
            <input type="hidden" name="id" value="<?= $p->user_id; ?>">
            <input type="text" name="nama" class="form-control" value="<?= $p->user_nama; ?>" required>
          </div>
          <div class="form-group">
            <label>Username</label>
            <input type="text" name="username" class="form-control" value="<?= $p->user_username; ?>" readonly>
          </div>
          <div class="form-group">
            <label>Password</label>
            <input type="text" name="password" class="form-control" placeholder="Kosongkan jika tidak ingin mengganti password">
          </div>
          <div class="form-group">
            <label>Nomor HP</label>
            <input type="number" name="noHP" class="form-control" value="<?= $p->user_noHP; ?>" required>
          </div>
          <div class="form-group">
            <label>Email</label>
            <input type="email" name="email" class="form-control" value="<?= $p->user_email; ?>" required>
          </div>
          <input type="submit" value="Simpan" class="btn btn-success btn-sm">
          <?= form_close(); ?>
        </div>
      </div>
    </div>
  </div>
</div>