<?= $this->session->flashdata('pesan'); ?>
<div class="container-fluid">
  <h4>Edit data petugas</h4>
  <div class="row">
    <div class="col-md-6">
      <div class="card">
        <div class="card-body">
          <?= form_open('updatePetugas'); ?>
          <div class="form-group">
            <label>Nama</label>
            <input type="hidden" name="id" value="<?= $p->user_id; ?>">
            <input type="text" name="nama" class="form-control" value="<?= $p->user_nama; ?>" required>
          </div>
          <div class="form-group">
            <label>Username</label>
            <input type="text" name="username" class="form-control" value="<?= $p->user_username; ?>" readonly>
          </div>
          <div class="form-group">
            <label>Password</label>
            <input type="text" name="password" class="form-control" placeholder="Kosongkan jika tidak ingin mengganti password">
          </div>
          <div class="form-group">
            <label>Role</label>
            <select name="role" class="form-control" required>
              <option disabled selected>-- Pilih hak akses --</option>
              <option <?php if($p->user_role == 1) { echo 'selected'; } ?> value="1">Admin</option>
              <option <?php if($p->user_role == 2) { echo 'selected'; } ?> value="2">Petugas</option>
              <option <?php if($p->user_role == 3) { echo 'selected'; } ?> value="3">User</option>
            </select>
          </div>
          <div class="form-group">
            <label>Nomor HP</label>
            <input type="number" name="noHP" class="form-control" value="<?= $p->user_noHP; ?>" required>
          </div>
          <div class="form-group">
            <label>Email</label>
            <input type="email" name="email" class="form-control" value="<?= $p->user_email; ?>" required>
          </div>
          <input type="submit" value="Simpan" class="btn btn-success btn-sm">
          <?= form_close(); ?>
        </div>
      </div>
    </div>
  </div>
</div>