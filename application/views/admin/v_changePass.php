<?= $this->session->flashdata('pesan'); ?>
<div class="container-fluid">
  <div class="row">
    <div class="col-md-6 mx-auto">
      <div class="card">
        <div class="card-body">
          <?= form_open('gantiPassword'); ?>
            <div class="form-group">
              <label>Masukan Password Lama</label>
              <input type="password" name="passLama" class="form-control">
              <?= form_error('passLama','<small class="text-danger">','</small>'); ?>
            </div>
            <div class="form-group">
              <label>Masukan Password Baru</label>
              <input type="password" name="passBaru" class="form-control">
              <?= form_error('passBaru','<small class="text-danger">','</small>'); ?>
            </div>
            <div class="form-group">
              <label>Masukan Ulang Password Baru</label>
              <input type="password" name="passBaru1" class="form-control">
              <?= form_error('passBaru1','<small class="text-danger">','</small>'); ?>
            </div>
            <input type="submit" value="Simpan" class="btn btn-success btn-sm">
          <?= form_close(); ?>
        </div>
      </div>
    </div>
  </div>
</div>