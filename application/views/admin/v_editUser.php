<div class="container-fluid">
  <h4>Edit profil</h4>
  <div class="row">
    <div class="col-md-6">
      <div class="card">
        <div class="card-body">
          <?= form_open('editUser'); ?>
            <div class="form-group">
              <label>Nama</label>
              <input type="text" name="nama" class="form-control" value="<?= $u->user_nama; ?>" required>
              <input type="hidden" name="id" value="<?= $u->user_id; ?>">
              <?= form_error('nama','<small class="text-danger">','</small>'); ?>
            </div>
            <div class="form-group">
              <label>No ID</label>
              <input type="text" name="noId" class="form-control" value="<?= $u->user_noId; ?>" disabled>
            </div>
            <div class="form-group">
              <label>Username</label>
              <input type="text" name="username" class="form-control" value="<?= $u->user_username; ?>" disabled>
            </div>
            <div class="form-group">
              <label>Email</label>
              <input type="email" name="email" class="form-control" value="<?= $u->user_email; ?>" required>
              <?= form_error('email','<small class="text-danger">','</small>'); ?>
            </div>
            <div class="form-group">
              <label>Nomor HP</label>
              <input type="number" name="noHP" class="form-control" value="<?= $u->user_noHP; ?>" required>
              <?= form_error('noHP','<small class="text-danger">','</small>'); ?>
            </div>
            <input type="submit" value="Simpan" class="btn btn-success btn-sm">
          <?= form_close(); ?>
        </div>
      </div>
    </div>
  </div>
</div>