<?= $this->session->flashdata('pesan'); ?>
<div class="container-fluid">
  <h4>Data Peminjaman</h4>
  <div class="card">
    <div class="card-body">
      <a href="" class="btn btn-primary btn-sm mb-3" data-toggle="modal" data-target="#add">Tambah data peminjaman</a>
      <div class="table-responsive">
        <table class="table table-bordered table-hover" id="data">
          <thead>
            <tr>
              <th>No</th>
              <th>No Peminjaman</th>
              <th>User Peminjam</th>
              <th>Buku</th>
              <th>Jumlah Pinjaman</th>
              <th>Tanggal Meminjam</th>
              <th>Tanggal Pengembalian</th>
              <th>Tanggal Dikembalikan</th>
              <th>Denda</th>
              <th>Status</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php $no=1; foreach($peminjaman as $p) { ?>
            <tr>
              <td><?= $no++; ?></td>
              <td><?= 'PMJ-'.$p->peminjaman_noId; ?></td>
              <td><?= $p->user_nama; ?></td>              
              <td><?= $p->buku_judul; ?></td>
              <td><?= $p->peminjaman_jumlah; ?></td>
              <td><?= date('d M Y', strtotime($p->peminjaman_dari)); ?></td>
              <td><?= date('d M Y', strtotime($p->peminjaman_sampai)); ?></td>
              <td><?php
                if($p->peminjaman_kembali == '0000-00-00') {
                  echo "Masih dipinjam";
                } else {
                  echo date('d M Y', strtotime($p->peminjaman_kembali));
                }
              ?></td>
              <td>Rp. <?= number_format($p->peminjaman_denda,'0',',','.'); ?></td>
              <td>
                <?php
                  if($p->peminjaman_status == 1) {
                    echo '<div class="badge badge-info">Masih dipinjam</div>';
                  } elseif($p->peminjaman_status == 2) {
                    echo '<div class="badge badge-info">Dikembalikan</div>';
                  } elseif($p->peminjaman_status == 3) {
                    echo '<div class="badge badge-danger">Dibatalkan</div>';
                  }
                ?>
              </td>
              <td>
                <?php
                  if($p->peminjaman_status == 1) {
                ?>
                <a href="<?= base_url('kembaliPem/'.$p->peminjaman_id); ?>" class="btn btn-info btn-sm">Dikembalikan</a>
                <a href="<?= base_url('batalkanPem/'.$p->peminjaman_id); ?>" class="btn btn-danger btn-sm">Dibatalkan</a>
                <?php } elseif($p->peminjaman_status != 1) { ?>
                <a href="<?= base_url('hapusPem/'.$p->peminjaman_id); ?>" class="btn btn-danger btn-sm">Hapus</a>
                <?php } ?>
              </td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>

      <h4 class="mt-4">Tabel Denda Harian</h4>
      <table class="table table-bordered table-hover">
        <thead>
          <tr>
            <th>No</th>
            <th>Harga Denda</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>Rp. <?= number_format($d->denda_harga,'0',',','.'); ?></td>
            <td>
              <a href="<?= base_url('denda'); ?>" class="btn btn-info btn-sm">Edit</a>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>

<div class="modal fade" id="add">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5>Tambah Menu</h5>
        <button type="button" data-dismiss="modal" class="close">&times;</button>
      </div>
      <div class="modal-body">
        <?= form_open('addPeminjaman'); ?>
        <div class="form-group">
          <label>User</label>
          <select name="user" class="form-control">
            <option disabled selected>-- Pilih User --</option>
            <?php foreach($user as $u) { ?>
            <option value="<?= $u->user_id; ?>"><?= $u->user_nama; ?></option>
            <?php } ?>
          </select>
        </div>
        <div class="form-group">
          <label>Buku</label>
          <select name="buku" class="form-control">
            <option disabled selected>-- Pilih Buku --</option>
            <?php foreach($buku as $b) { ?>
            <option value="<?= $b->buku_id; ?>"><?= $b->buku_judul; ?></option>
            <?php } ?>
          </select>
        </div>
        <div class="form-group">
          <label>Jumlah Peminjaman</label>
          <input type="number" name="jumlah" class="form-control" value="1">
        </div>
        <div class="form-group">
          <label>Tanggal Peminjaman</label>
          <input type="date" name="pinjam" class="form-control">
        </div>
        <div class="form-group">
          <label>Tanggal Dikembalikan</label>
          <input type="date" name="kembali" class="form-control">
        </div>
        <input type="submit" value="Simpan" class="btn btn-success btn-sm">
        <?= form_close() ?>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">Close</button>
      </div>
    </div>
  </div>
</div>