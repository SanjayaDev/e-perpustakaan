<?= $this->session->flashdata('pesan'); ?>
<div class="container-fluid">
  <h4>Sub Menu Management</h4>
  <div class="card">
    <div class="card-body">
      <a href="" class="btn btn-primary btn-sm mb-3" data-toggle="modal" data-target="#add">Tambah Sub Menu</a>
      <div class="table-responsive">
        <table class="table table-bordered table-hover" id="data">
          <thead>
            <tr>
              <th>No</th>
              <th>Sub Menu</th>
              <th>Judul</th>
              <th>Link</th>
              <th>Icon</th>
              <th>Status</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php $no=1; foreach($sub as $s) { ?>
            <tr>
              <td><?= $no++; ?></td>
              <td><?= $s->menu_judul; ?></td>
              <td><?= $s->sub_judul; ?></td>
              <td><?= $s->sub_link; ?></td>
              <td><?= $s->sub_icon; ?></td>
              <td>
                <?php
                  if($s->sub_status == 1) {
                    echo '<div class="badge badge-success">Aktif</div>';
                  } elseif($s->sub_status == 2) {
                    echo '<div class="badge badge-danger">Tidak aktif</div>';
                  }
                ?>
              </td>
              <td>
                <a href="<?= base_url('editSub/'.$s->sub_id); ?>" class="btn btn-info btn-sm">Edit</a>
                <a href="<?= base_url('hapusSub/'.$s->sub_id); ?>" class="btn btn-danger btn-sm">Hapus</a>
              </td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="add">
  <div class="modal-dialog modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h5>Tambah Menu</h5>
        <button type="button" data-dismiss="modal" class="close">&times;</button>
      </div>
      <div class="modal-body">
        <?= form_open('addSubMenu'); ?>
        <div class="form-group">
          <label>Menu</label>
          <select name="menu" class="form-control" required>
            <option selected disabled>-- Pilih menu </option>
            <?php foreach($listMenu as $l) { ?>
            <option value="<?= $l->menu_id; ?>"><?= $l->menu_judul; ?></option>
            <?php } ?>
          </select>
        </div>
        <div class="form-group">
          <label>Judul Submenu</label>
          <input type="text" name="judul" class="form-control" required>
        </div>
        <div class="form-group">
          <label>Link</label>
          <input type="text" name="link" class="form-control" required>
        </div>
        <div class="form-group">
          <label>Icon</label>
          <input type="text" name="icon" class="form-control" required>
        </div>
        <div class="form-group">
          <label>Status</label>
          <select name="status" class="form-control" required>
            <option disabled selected>-- Pilih status --</option>
            <option value="1">Aktif</option>
            <option value="2">Tidak aktif</option>
          </select>
        </div>
        <input type="submit" value="Simpan" class="btn btn-success btn-sm">
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-danger btn-sm">Close</button>
      </div>
    </div>
  </div>
</div>