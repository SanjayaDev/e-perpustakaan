<?= $this->session->flashdata('pesan'); ?>
<div class="container-fluid">
  <div class="row">
    <div class="col-md-4">
      <div class="card">
        <div class="card-body">
          <?= form_open('prosesEditAccess'); ?>
            <div class="form-group">
              <label>Menu judul</label>
              <input type="hidden" name="id" value="<?= $acc->access_id; ?>">
              <select name="menu" class="form-control" required>
                <?php foreach($listMenu as $l) { ?>
                  <option <?php if($l->menu_id == $acc->menu_id ){ echo "selected"; } ?> value="<?= $l->menu_id ?>"><?= $l->menu_judul; ?></option>
                <?php } ?>
              </select>
              <?= form_error('menu','<small class="text-danger">','</small>') ?>              
            </div>
            <div class="form-group">
              <label>Role</label>
              <select name="role" class="form-control" required>
                <option <?php if($acc->role_id == 1 ) { echo 'selected'; } ?> value="1">Admin</option>
                <option <?php if($acc->role_id == 2 ) { echo 'selected'; } ?> value="2">Petugas</option>
                <option <?php if($acc->role_id == 3 ) { echo 'selected'; } ?> value="3">User</option>
              </select>
            </div>
            <input type="submit" value="Simpan" class="btn btn-success btn-sm">
          <?= form_close(); ?>
        </div>
      </div>
    </div>
  </div>
</div>