<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <style>
    * {
      padding: 0;
      margin: 0;
      box-sizing: border-box;
    }
    .kotak {
      width: 400px;
      border: 1px solid black;
    }

    .kotak header{
      text-align: center;
    }

    .kotak header h5 {
      font-size: 25px;
      font-family:'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;
    }

    .kotak header p {
      font-size: 13px;
      font-family:'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;
    }

    .kotak table {
      width: 100%;
      padding: 10px;
    }

    .kotak table tr th {
      text-align: left;
    }
  </style>
  <title><?= $title; ?></title>
</head>
<body>
  
<div class="kotak">
  <header>
    <h5>Perpustakaan Digital</h5>
    <p>Jl. Abc, Kec Abc</p>
  </header>
  <hr>
  <table>
    <tr>
      <th>No Anggota</th>
      <td>PPD-<?= $u->user_noId; ?></td>
    </tr>
    <tr>
      <th>Nama Anggota</th>
      <td><?= $u->user_nama; ?></td>
    </tr>
    <tr>
      <th>Email</th>
      <td><?= $u->user_email; ?></td>
    </tr>
  </table>
</div>


<script>
  window.print();
</script>
</body>
</html>