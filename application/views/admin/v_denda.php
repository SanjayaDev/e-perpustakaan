<div class="container-fluid">
  <div class="row">
    <div class="col-md-4">
      <div class="card">
        <div class="card-body">
          <?= form_open('denda'); ?>
            <div class="form-group">
              <label>Harga Denda</label>
              <input type="text" name="denda" value="<?= $d->denda_harga; ?>" class="form-control">
              <?= form_error('denda','<small class="text-danger">','<small>'); ?>
            </div>
            <input type="submit" value="Simpan" class="btn btn-success btn-sm">
          <?= form_close(); ?>
        </div>
      </div>
    </div>
  </div>
</div>