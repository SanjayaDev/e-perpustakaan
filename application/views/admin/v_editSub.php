<?= $this->session->flashdata('pesan'); ?>
<div class="container-fluid">
  <h5>Edit Sub Menu</h5>
  <div class="row">
    <div class="col-md-8">
      <div class="card">
        <div class="card-body">
          <?= form_open('updateSub'); ?>
            <div class="form-group">
              <label>Menu</label>
              <input type="hidden" name="id" value="<?= $s->sub_id; ?>">
              <select name="menu" class="form-control" required>
                <option selected disabled>-- Pilih menu </option>
                <?php foreach($listMenu as $l) { ?>
                <option <?php if($l->menu_id == $s->sub_menu) { echo "selected"; } ?> value="<?= $l->menu_id; ?>"><?= $l->menu_judul; ?></option>
                <?php } ?>
              </select>
            </div>
            <div class="form-group">
              <label>Judul Submenu</label>
              <input type="text" name="judul" class="form-control" value="<?= $s->sub_judul; ?>" required>
            </div>
            <div class="form-group">
              <label>Link</label>
              <input type="text" name="link" class="form-control" value="<?= $s->sub_link; ?>" required>
            </div>
            <div class="form-group">
              <label>Icon</label>
              <input type="text" name="icon" class="form-control" value="<?= $s->sub_icon; ?>" required>
            </div>
            <div class="form-group">
              <label>Status</label>
              <select name="status" class="form-control" required>
                <option disabled selected>-- Pilih status --</option>
                <option <?php if($s->sub_status == 1) { echo "selected"; } ?> value="1">Aktif</option>
                <option <?php if($s->sub_status == 2) { echo "selected"; } ?> value="2">Tidak aktif</option>
              </select>
            </div>
            <input type="submit" value="Simpan" class="btn btn-success btn-sm">
          <?= form_close(); ?>
        </div>
      </div>
    </div>
  </div>
</div>