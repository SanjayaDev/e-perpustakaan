<?php

  function protek_login()
  {
    $ci = get_instance();

    if(!$ci->session->userdata('status')) {
      $ci->session->set_flashdata('pesan', '<script>sweet("Gagal Masuk!","Wajib login terlebih dahulu!","error","Tutup");</script>');
      
      redirect('login');
    }
  }

?>