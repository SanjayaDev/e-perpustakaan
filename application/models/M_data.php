<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_data extends CI_Model {
  

  public function insertData($data,$table) {
    $this->db->insert($table,$data);
  }

  public function getData($table)
  {
    return $this->db->get($table);
  }

  public function editData($where,$table) {
    return $this->db->where($where)->get($table);
  }

  public function updateData($data,$where,$table)
  {
    $this->db->where($where)->update($table,$data);
  }

  public function deleteData($where,$table)
  {
    $this->db->delete($table,$where);
  }

  public function joinAcc()
  {
    return $this->db->select('*')
                    ->from('tb_access,tb_menu')
                    ->where('tb_access.menu_id = tb_menu.menu_id')
                    ->get();
  }

  public function joinMenu()
  {
    return $this->db->select('*')
                    ->from('tb_menu')
                    ->join('tb_access','tb_access.menu_id = tb_menu.menu_id')
                    ->where(['tb_access.role_id' => $this->session->userdata('role')])
                    ->get();
  }

  public function joinSub()
  {
    return $this->db->select('*')
                    ->from('tb_sub,tb_menu')
                    ->where('tb_sub.sub_menu = tb_menu.menu_id')
                    ->get();
  }

  public function joinBuku()
  {
    return $this->db->select('*')
                    ->from('tb_buku,tb_penerbit,tb_kategori')
                    ->where('tb_buku.buku_penerbit=tb_penerbit.penerbit_id')
                    ->where('tb_buku.buku_kategori=tb_kategori.kategori_id')
                    ->get();
  }

  public function joinPeminjaman()
  {
    return $this->db->select('*')
                    ->from('tb_peminjaman,tb_user,tb_buku')
                    ->where('tb_peminjaman.peminjaman_user=tb_user.user_id')
                    ->where('tb_peminjaman.peminjaman_buku=tb_buku.buku_id')
                    ->get();
  }
}